<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * @Route("admin/db")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class InitilizeDatabaseController extends Controller
{
    /**
     * @Route("/", name="admin_database_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {

       echo "All done.";
       die;
    }

    /**
     * @Route("/insert-events", name="admin_initialize_events")
     */
    public function insertEventsAction(Request $request)
    {
       echo "Inserting Events";
       $em = $this->getDoctrine()->getManager();

       // get current competition
       $competition = $em->getRepository('AppBundle:Competition')->findOneByCurrent(1);



       dump($competition);
       echo "All done.";
       die;
    }

    /**
     * @Route("/insert-workouts", name="admin_dummy_workouts")
     */
    public function insertDummyDataAction(Request $request)
    {

       echo "All done.";
       die;
    }


}
