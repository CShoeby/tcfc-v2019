<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Competition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Workout;
use AppBundle\Entity\Event;
use AppBundle\Entity\Profile;
use AppBundle\Entity\User;
use AppBundle\Service\CompetitionHelper;

/**
 * Competition controller.
 *
 * @Route("competitor")
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 */
class CompetitorController extends Controller
{
    /**
     * Lists all competitors.
     *
     * @Route("/", name="competitors")
     * @Method("GET")
     */
    public function indexAction(CompetitionHelper $competitionHelper)
    {
        // show a page if not logged in about the competition AND a register link
        if (!$this->getParameter('isCompetitionOpen')) {
            return $this->render('default/competitionClosed.html.twig'); 
        }
 
        $em = $this->getDoctrine()->getManager();
        $competitors = $em->getRepository('AppBundle:User')->findAll();
        
        return $this->render('competitor/index.html.twig', 
            array(
            'competitors' => $competitors,
            'competition' => $competitionHelper->getCurrentCompetition(),
        ));
    }

    /**
     * View competitor
     *
     * @Route("/{competitor}", name="competitor_show")
     * @Method("GET")
     */
    public function viewAction(User $competitor, CompetitionHelper $competitionHelper)
    {
        // show a page if not logged in about the competition AND a register link
        if (!$this->getParameter('isCompetitionOpen')) {
            return $this->render('default/competitionClosed.html.twig'); 
        }
 
        $em = $this->getDoctrine()->getManager();
        $competition = $competitionHelper->getCurrentCompetition();
        $competitorData = $competitionHelper->getCompetitorData($competitor, $competition);

        return $this->render('dashboard/index.html.twig', 
            array(
                'competitor'            => $competitor,
                'challenges'            => $competitionHelper->getAllChallenges($competition, $limit = 2),
                'totalChallengesToDate' => $competitionHelper
                                                ->getTotalChallengesToDate($competition),
                'workouts'              => $competitorData['workouts'],
                'personalBests'         => $competitorData['personalBests'],
                'challengesCompleted'   => $competitorData['challengesCompleted'],
                'numMissedChallenges'   => $competitorData['numMissedChallenges'],
                'dashboardTitle'        => 'Competitor: '.$competitor->getUsername(),
                'numImprovedChallenges' => $competitorData['numImprovedChallenges']

        ));
    }
}
