<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Competition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Workout;
use AppBundle\Entity\Event;
use AppBundle\Entity\Profile;
use AppBundle\Service\CompetitionHelper;

/**
 * Competition controller.
 *
 * @Route("dashboard")
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 */
class DashboardController extends Controller
{
    /**
     * Lists all competition entities.
     *
     * @Route("/", name="dashboard")
     * @Method("GET")
     */
    public function indexAction(CompetitionHelper $competitionHelper)
    {
        // show a page if not logged in about the competition AND a register link
        if (!$this->getParameter('isCompetitionOpen')) {
            return $this->render('default/competitionClosed.html.twig'); 
        }

        $em = $this->getDoctrine()->getManager();
        $competition = $competitionHelper->getCurrentCompetition();
        $competitorData = $competitionHelper->getCompetitorData($this->getUser(), $competition);


        return $this->render('dashboard/index.html.twig', 
            array(
                'competitor'            => $this->getUser(),
                'challenges'            => $competitionHelper->getAllChallenges($competition, $limit = 2),
                'totalChallengesToDate' => $competitionHelper
                                                ->getTotalChallengesToDate($competition),
                'workouts'              => $competitorData['workouts'],
                'personalBests'         => $competitorData['personalBests'],
                'challengesCompleted'   => $competitorData['challengesCompleted'],
                'numMissedChallenges'   => $competitorData['numMissedChallenges'],
                'numImprovedChallenges' => $competitorData['numImprovedChallenges']
        ));
    }

    /**
     * 1st page for creating a new workout
     *
     * @Route("/enter-workout/for-event", name="profile_for_event_workout_new")
     * @Method({"GET", "POST"})
     */
    public function chooseEventAction(Request $request) 
    {
        // get current competition

        // get events for current competition
        $events = array();
        $em = $this->getDoctrine()->getManager();

        // $events = $em->getRepository('AppBundle:Event')->findAll();
        $competition = $em->getRepository('AppBundle:Competition')->findOneByCurrent(1);

        $events = $em->getRepository('AppBundle:Event')->findByCanEnterWorkout($competition, $this->getUser()->getGender()); 
        return $this->render('dashboard/chooseEvent.html.twig', array(
            'events' => $events,
        ));
    }

    /**
     * Creates a new workout entity.
     *
     * @Route("/enter-workout/for-{eventKey}", name="profile_workout_new")
     * @Route("/enter-alt-workout/for-{eventKey}", name="profile_alt_workout_new")
     * @Method({"GET", "POST"})
     */
    public function enterWorkoutAction(Request $request, CompetitionHelper $competitionHelper, $eventKey) 
    {
        // dump($request->get('_route')); die;
        $em = $this->getDoctrine()->getManager();
        $allChallenges = $competitionHelper->getAllChallenges();
        if (!array_key_exists($eventKey, $allChallenges)) {
            throw new Exception("Something went wrong", 1);   
        }
        // dump($event);die;
        if ($request->get('_route') == "profile_workout_new") {
            $event = $em->getRepository('AppBundle:Event')->find($allChallenges[$eventKey]["id"]);
        }

        else {
            $event = $em->getRepository('AppBundle:Event')->find(10);
            $altForEvent = $em->getRepository('AppBundle:Event')
                    ->find($allChallenges[$eventKey]["id"]);
            $event->setAlternateForEvent($altForEvent);
        }

        $workout = new Workout($this->getUser(), $event, $allChallenges[$eventKey]["startDate"]);
        $form = $this->createForm('AppBundle\Form\ProfileWorkoutType', $workout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $em = $this->getDoctrine()->getManager();
            $workout->setEnteredOn(new \DateTime());
            $em->persist($workout);
            $em->flush();

            return $this->redirectToRoute('dashboard');
        }

        return $this->render('dashboard/enterWorkout.html.twig', array(
            'workout' => $workout,
            'event' => $event,
            'eventKey' => $eventKey,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing workout entity.
     *
     * @Route("/{id}/edit", name="dashboard_workout_edit")
     * @Method({"GET", "POST"})
     */
    public function editWorkoutAction(Request $request, Workout $workout)
    {
        // if workout->user != current user redirect to dashboard
        if ($workout->getUser()->getId() != $this->getUser()->getId()) {
            return $this->redirectToRoute('dashboard');
        }

        $editForm = $this->createForm('AppBundle\Form\ProfileWorkoutType', $workout);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dashboard');
        }

        return $this->render('dashboard/workoutEdit.html.twig', array(
            'workout' => $workout,
            'form' => $editForm->createView(),
        ));
    }
}
