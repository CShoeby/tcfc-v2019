<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Competition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Service\CompetitionHelper;

/**
 * Competition controller.
 *
 * @Route("standings")
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 */
class StandingsController extends Controller
{
    /**
     * Lists all competition entities.
     *
     * @Route("/", name="standings")
     * @Method("GET")
     */
    public function indexAction(CompetitionHelper $competitionHelper)
    {
        // show a page if not logged in about the competition AND a register link
        if (!$this->getParameter('isCompetitionOpen')) {
            return $this->render('default/competitionClosed.html.twig'); 
        }

        $em = $this->getDoctrine()->getManager();

        $competition = $competitionHelper->getCurrentCompetition();
        return $this->render('standings/index.html.twig', array(
            'competition' => $competition,
            'events' => $competition->getEvents(),
            'allStandings' => $competitionHelper->getAllStandings($competition),
        ));
    }

}
