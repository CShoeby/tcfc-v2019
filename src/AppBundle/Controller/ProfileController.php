<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Competition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Workout;
use AppBundle\Entity\Event;
use AppBundle\Entity\Profile;

/**
 * Competition controller.
 *
 * @Route("profile")
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 */
class ProfileController extends Controller
{

    /**
     * Displays a form to edit an a user's profile entity.
     *
     * @Route("/edit", name="user_edit_profile")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        $user = $this->getUser();

        $profile = $user->getProfile();

        $editForm = $this->createForm('AppBundle\Form\ProfileType', $profile);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($profile);
            $em->flush();

            return $this->redirectToRoute('dashboard');
        }

        return $this->render('profile/edit.html.twig', array(
            'profile' => $this->getUser()->getProfile(),
            'edit_form' => $editForm->createView(),
        ));
    }

}
