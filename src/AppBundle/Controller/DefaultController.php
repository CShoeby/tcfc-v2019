<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Service\CompetitionHelper;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        // show a page if not logged in about the competition AND a register link
        if (!$this->getParameter('isCompetitionOpen')) {
            return $this->render('default/competitionClosed.html.twig'); 
        }

        // if logged in send to profile page
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('dashboard');
        }

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/rules", name="rules_page")
     */
    public function rulesAction(Request $request)
    {
        if (!$this->getParameter('isCompetitionOpen')) {
            return $this->render('default/competitionClosed.html.twig'); 
        }

        return $this->render('default/rules.html.twig');
    }

    /**
     * @Route("/about", name="about_page")
     */
    public function aboutAction(Request $request)
    {
        if (!$this->getParameter('isCompetitionOpen')) {
            return $this->render('default/competitionClosed.html.twig'); 
        }
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route("/faq", name="faq_page")
     */
    public function faqAction(Request $request)
    {
        if (!$this->getParameter('isCompetitionOpen')) {
            return $this->render('default/competitionClosed.html.twig'); 
        }
        return $this->render('default/faq.html.twig');
    }

    /**
     * @Route("/challenges", name="challenges_page")
     */
    public function challengesAction(Request $request, CompetitionHelper $ch)
    {
        if (!$this->getParameter('isCompetitionOpen')) {
            return $this->render('default/competitionClosed.html.twig'); 
        }
        return 
        $this->render('default/challenges.html.twig', array(
            'competition' => $ch->getCurrentCompetition(), 
            'challenges' => $ch->getAllChallenges(),

            ));
    }

}
