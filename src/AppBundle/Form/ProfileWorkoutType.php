<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProfileWorkoutType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $event = $builder->getData()->getEvent();

        $builder
            ->add('workoutDate', DateType::class, [
                    'disabled' => true,
                    'attr' => array(
                        'readonly' => true,

                    ),
                ]) 
            ;

        // change value input based on event units
        if ($event->getUnitLabel() == 'seconds') {
            $builder
                ->add('valueSeconds', DateIntervalType::class, array(
                    'with_years'  => false,
                    'with_months' => false,
                    'with_days'   => false,
                    'with_minutes'   => true,
                    'with_seconds'  => true,
                    'widget' => 'text',
                    'label' => $event->getQuestionForForm(),
                ));
        } 
        else
        {
            $builder
                ->add('value', null, array(
                    'label' => $event->getQuestionForForm()." (in ". $event->getUnitLabel().")"
                ));
        }

        $builder
            ->add('description', TextareaType::class, array(
                    'label' => 'How\'d it go?',
                    'required' => false
                ))
            ->add('save', SubmitType::class, array(
                    'attr' => array('class' => 'save btn-primary'),
                ))
            ;


        // for alternate workouts override description and value fields
        if (!is_null($event->getAlternateForEvent())) {
            $builder->remove('value');
            $builder
                ->add('description', TextareaType::class, array(
                    'label' => $event->getQuestionForForm(),
                    'required' => false
                ));

        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Workout'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile_workout';
    }


}
