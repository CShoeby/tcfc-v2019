<?php
namespace AppBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Event;
use AppBundle\Entity\Workout;
use AppBundle\Service\WorkoutHelper;


class CompetitionHelper {

    private $orm;

    public function __construct(ObjectManager $objectManager) {
        $this->orm = $objectManager;
    }

    public function getTotalChallengesToDate($competition = null) {
        $competition = !is_null($competition) ? 
            $competition : $this->getCurrentCompetition();

        $allChallenges = $this->getAllChallenges($competition);
        $currentDate = date("Y-m-d 00:00:00");
        // testing date
        // $currentDate = date("Y-m-05 00:00:00");

        $challengesToDate = array();
        foreach ($allChallenges as $key => $event) {
            if ($event["startDate"] < $currentDate) {
               $challengesToDate[$key] = $event;
               // var_dump($event);
            }
        }   
        
        return $challengesToDate;
    }

    public function getAllChallenges($competition = null, $limit = null) {
        $competition = !is_null($competition) ? 
            $competition : $this->getCurrentCompetition();

        $sql = "
            SELECT e.id, e.number, e.title, e.startDate, e.description, e.eventType, e.repeatEveryNDays, e.repeatNTimes, e.isDisplayed
            FROM event as e
            Where eventType = 4
            and competition_id = :competitionId
                "
        ;

        $statement = $this->orm->getConnection()->prepare($sql);
        // Set parameters 
        $statement->bindValue('competitionId', $competition->getId());
        $statement->execute();

        $result = $statement->fetchAll();

        $currentDayOfMonth = date('d');
        $totalChallenges = array();
        foreach ($result as $key => $event) {
            $repeatNTimes = $event['repeatNTimes'];
            $repeatEveryNDays = $event['repeatEveryNDays'];

            $startDate = $event['startDate'];
            $beginningStartDateStr = substr($startDate, 0, 8);
            $dayStr = substr($startDate, 8, 2);
            $endOfStartDateStr = substr($startDate, 10);

            for ($i=0; $i < $event['repeatNTimes'] ; $i++) { 
                $day = (int)$dayStr + $i*$event['repeatEveryNDays'];

                // if limit is set, grab only current day plus limit to the array
                if(!is_null($limit) && 
                    (
                        $day > ($currentDayOfMonth + $limit) ||
                        $day <= ($currentDayOfMonth - $limit)
                    )) {
                    continue;
                }

                if ($day < 10) {
                    $day = "0".$day;
                }

                $event['startDate'] = 
                    $beginningStartDateStr
                    . $day 
                    . $endOfStartDateStr;
                $totalChallenges['event-'.$event["number"]."-". ($i+1)] = $event;
            }
        }

        // sort by day
        array_multisort(array_map(function($element) {
              return $element['startDate'];
          }, $totalChallenges), SORT_ASC, $totalChallenges);

        return $totalChallenges;

    }

    public function getAllStandings($competition = null) {

        $competition = !is_null($competition) ? 
            $competition : $this->getCurrentCompetition();

        $allStandings = array();
        
        $events = $competition->getEvents();

        foreach ($events as $event) {
            $eventId = $event->getId();
            $allStandings[$eventId] = array();
            // dump($event->getEventType()->getName());
            if ($event->getEventType()->getName() == 'single value') {
                $order = $event->getIsGreatestValue() ? 'max' : 'min';

                $allStandings[$eventId] = 
                    $this->getEventStandingsSingleValue($eventId, $order);
            }

            if ($event->getEventType()->getName() == 'most challenges completed' ) {
                $allStandings[$eventId] = $this->getMostChallengesCompletedStandings($competition);
            }
            if ($event->getEventType()->getName() == 'most improved' ) {
                $allStandings[$eventId] = $this->getMostImprovedStandings($competition);
            }
        }

        return $allStandings;  
    }

    public function getEventStandingsSingleValue($eventId, $order = 'max') {
        
        $order = $order == 'max' ? 'max':'min';

        $sql = "
            SELECT `user_id`, `event_id`, `u`.`username`,
                (CASE e.isGreatestValue 
                   When 1 THEN max(value)
                   When 0 THEN min(value)
                   END) as value
             FROM workout as w
                Join fos_user as u on `u`.`id` = `w`.`user_id`
                JOIN event as e on `e`.`id` = `w`.`event_id`
                Where `w`.`event_id` = :eventId
                and `w`.`value` <> 0
                Group by `user_id`
                Order by value "
            ;
        $sql .= $order == 'max' ? 'desc':'asc';

        $statement = $this->orm->getConnection()->prepare($sql);
        // Set parameters 
        $statement->bindValue('eventId', $eventId);
        $statement->execute();

        $result = $statement->fetchAll();

        return $this->assignRanksToStandings($result);
    }

    public function getImprovedScoreForUser($user, $competition) {

        $sqlSetRowNum = "SET @row_number = 0; ";
        $sql = "SELECT 
        (@row_number:=@row_number + 1) AS num,
        `user_id`, `event_id`, `u`.`username`, `e`.unitLabel, workoutDate, 
               (CASE e.isGreatestValue 
                   When 1 THEN max(value)
                   When 0 THEN min(value)
                   END) as value
            FROM workout as w
                Join fos_user as u on `u`.`id` = `w`.`user_id`
                Join event as e on e.id = w.event_id
            WHERE user_id = :userId
            AND value <> 0
                Group by event_id, workoutDate
            ORDER BY event_id, workoutDate ASC 
                    "
            ;
       

        $statement = $this->orm->getConnection()->prepare($sqlSetRowNum);
        $statement = $this->orm->getConnection()->prepare($sqlSetRowNum);
        $statement->execute();

        $statement = $this->orm->getConnection()->prepare($sql);
        // Set parameters 
        $statement->bindValue('competitionId', $competition->getId());
        $statement->bindValue('userId', $user->getId());
        $statement->execute();

        $result = $statement->fetchAll();
        echo "<pre>";
        var_dump($result);
        echo "donezo";die;
        return $this->assignRanksToStandings($result);
    }

    public function getAllEventStandings($competition) {

       $sql = 
       "SELECT `user_id`, `event_id`, `u`.`username`, workoutDate,
           (CASE e.isGreatestValue 
           When 1 THEN max(value)
           When 0 THEN min(value)
           END) as value
             FROM workout as w
                Join fos_user as u on `u`.`id` = `w`.`user_id`
                Join event as e on e.id = w.event_id
            Where e.competition_id = :competitionId
            and w.value <> 0
                Group by `user_id`, event_id  
            ORDER BY `w`.`user_id` ASC";

    }

    private function assignRanksToStandings($standings) {
        $rankedStandings = array();

        $previousValue = null;
        $rank = 1;
        $numOfTiedCompetitors = 0;

        foreach ($standings as $key => $data) {

            if (is_null($previousValue)) {
                $previousValue = $data['value'];
            }
            if (!is_null($previousValue) && $previousValue != $data['value']) {
                $rank = $rank + $numOfTiedCompetitors;
                $numOfTiedCompetitors = 0;
                $previousValue = $data['value'];
            } 

            if ($previousValue == $data['value']) {
                $numOfTiedCompetitors++;
            }

            $data['rank'] = $rank;
            $rankedStandings[] = $data;
        }

        return $rankedStandings;
    }

    public function getMostChallengesCompletedStandings($competition) {

        $sql = "
        SELECT count(workoutId) as value, `user_id`, `username` FROM (
            SELECT  w.id as workoutId, `user_id`, `event_id`, `u`.`username`, workoutDate
             FROM workout as w
                Join fos_user as u on `u`.`id` = `w`.`user_id`
                Join  event as e on w.event_id = e.id
                -- Join competition as c on c.id = e.competition_id
                Where e.competition_id = :competitionId
                and w.value <> 0
                -- and user_id = 2
            Group by user_id, event_id, workoutDate
            ) as workoutsForChallenges
            group by user_id
            order by value desc
                "
        ;

        $statement = $this->orm->getConnection()->prepare($sql);
        // Set parameters 
        $statement->bindValue('competitionId', $competition->getId());
        $statement->execute();

        $result = $statement->fetchAll();

        return $this->assignRanksToStandings($result);
    }

    public function getMostImprovedStandings($competition, $user = null) {

        // each workout best value per workout day per user 
        // must do for greatest value events 

        $sql = "
            SELECT `user_id`, `username`,
                sum(didImprove) as value
                FROM 
                (SELECT  
                (CASE e.isGreatestValue 
                When 1 THEN 
                  (SELECT  max(value) as prevBestTemp
                      FROM workout as w2 
                      JOIN event e2 on w2.event_id = e2.id
                      where w2.workoutDate < w.workoutDate
                      and e2.id = w.event_id
                      and user_id = w.user_id
                      and w.value <> 0
                      ) < max(value) 
                WHEN 0 THEN 
                  (SELECT  min(value) as prevBestTemp
                      FROM workout as w2 
                      JOIN event e2 on w2.event_id = e2.id
                      where w2.workoutDate < w.workoutDate
                      and e2.id = w.event_id
                      and user_id = w.user_id
                      and w.value <> 0
                      ) > min(value) 
                END ) as didImprove, 
                    `user_id`, `event_id`, `u`.`username`, `e`.unitLabel, workoutDate,
                    (CASE e.isGreatestValue 
                           When 1 THEN max(value)
                           When 0 THEN min(value)
                           END) as t1value
                        FROM  
                        workout as w
                            Join fos_user as u on `u`.`id` = w.user_id
                            Join event as e on e.id = w.event_id
                        where e.competition_id = :competitionId 
                        and w.value <> 0
                        ";

                if (!is_null($user)) {
                    $sql .= " and w.user_id = :userId ";
                }

                $sql .= "Group by event_id, workoutDate, user_id
                ) as didImproveQuery
                Group by user_id
                HAVING value is not null
                AND value <> 0
                ORDER BY value desc
                "; 
 

        $statement = $this->orm->getConnection()->prepare($sql);
        // Set parameters 
        $statement->bindValue('competitionId', $competition->getId());
        
        if (!is_null($user)) {
            $statement->bindValue('userId', $user->getId());
        }
        $statement->execute();

        if (!is_null($user)) {
            $result = $statement->fetch();

            return $result ? $result["value"] : 0;
        }

        $result = $statement->fetchAll();
        return $this->assignRanksToStandings($result);
    }

    public function getChallengesCompletedByUser($user, $competition) {
        $userId = is_int($user) ? $user : $user->getId();

        $sql = "SELECT  w.id as workoutId, `user_id`, `event_id`, `u`.`username`, workoutDate
                 FROM workout as w
                    Join fos_user as u on `u`.`id` = `w`.`user_id`
                    Join  event as e on w.event_id = e.id
                    Join event_type as et on e.eventType = et.id
                    Where e.competition_id = :competitionId
                    and w.value <> 0
                    and user_id = :userId
                    and et.id <> 11
                Group by user_id, event_id, workoutDate
                "
            ;

        $statement = $this->orm->getConnection()->prepare($sql);
        // Set parameters 
        $statement->bindValue('competitionId', $competition->getId());
        $statement->bindValue('userId', $user->getId());
        $statement->execute();

        $result = $statement->fetchAll();

        return $result;
    }

    public function getCurrentCompetition(){

        return $this->orm->getRepository('AppBundle:Competition')->findOneByCurrent(1);
    }

    public function getPersonalBests($user, $competition = null) {
        $competition = !is_null($competition) ? 
                $competition : $this->getCurrentCompetition();

        $allPRs = array();
        
        $events = $competition->getEvents();

        foreach ($events as $event) {
            $eventId = $event->getId();
            $allPRs[$eventId] = null;

            if ($event->getEventType()->getName() == 'single value') {
                $order = $event->getIsGreatestValue() ? 'max' : 'min';

                $allPRs[$eventId] = 
                    $this->getPersonalBestForEvent($user, $eventId, $order);
            }
        }
        return $allPRs;  
    }

    public function getPersonalBestForEvent($user, $event, $order = 'max') {
        $userId = is_int($user) ? $user : $user->getId();
        $eventId = is_int($event) ? $event : $event->getId();

        $order = $order == 'max' ? 'max':'min';

        $sql = "
            SELECT `user_id`, `event_id`, `u`.`username`, `e`.`unitLabel`,
            $order(`value`) as value
             FROM workout as w
                Join fos_user as u on `u`.`id` = `w`.`user_id`
                Join event as e on `e`.`id` = `w`.`event_id`
                Where `event_id` = :eventId
                and `user_id` = :userId
                and `w`.`value` <> 0
                "
            ;

        $statement = $this->orm->getConnection()->prepare($sql);
        // Set parameters 
        $statement->bindValue('eventId', $eventId);
        $statement->bindValue('userId', $userId);
        $statement->execute();

        $result = $statement->fetchAll();
        $workout = is_array($result) ? $result[0] : $result;
        $workout["prettyValue"] = (new WorkoutHelper())->getPrettyValue($workout["value"], $workout["unitLabel"]);

        return $workout;
    }

    public function getWorkoutsForCompetitor($competitor) {
        $workoutRepo = $this->orm->getRepository('AppBundle:Workout');
        $qb = $workoutRepo->createQueryBuilder('w');
        $query = $qb
                    ->where('w.user = :competitor')
                    ->andWhere($qb->expr()->gt('w.value', 0))
                    ->setParameter('competitor', $competitor->getId())
                    ->orderBy('w.id', 'desc')
                    ->getQuery();

        return $query->getResult();
    }


    public function getCompetitorData($competitor, $competition) {
        $competition = !is_null($competition) ? 
                $competition : $this->getCurrentCompetition();
                
        $totalChallengesToDate = $this->getTotalChallengesToDate($competition);

        $workouts = $this->getWorkoutsForCompetitor($competitor);

        $personalBests = 
            $this->getPersonalBests($competitor, $competition);

        $challengesCompleted = 
            $this->getChallengesCompletedByUser($competitor, $competition);

        $numMissedChallenges = count($totalChallengesToDate) - count($challengesCompleted);
        // account for competition has not start yet 
        $numMissedChallenges = $numMissedChallenges < 0 ? 0 : $numMissedChallenges;
        $numImprovedChallenges = $this->getMostImprovedStandings($competition, $competitor);

        return array(
            'workouts' => $workouts,
            'personalBests' => $personalBests,
            'challengesCompleted' => $challengesCompleted,
            'numMissedChallenges' => $numMissedChallenges,
            'numImprovedChallenges' => $numImprovedChallenges,
        );
    }
}