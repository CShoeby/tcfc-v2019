<?php
namespace AppBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Event;
use AppBundle\Entity\Workout;

class WorkoutHelper {

    public function getPrettyValue($value, $unitLabel)
    {   
        if (is_null($value)) {
            return null;
        }

        if ( $unitLabel == "seconds") {
            //convert seconds to minutes and seconds
            $mins = floor($value / 60);
            $seconds = $value % 60;
            $seconds = strlen($seconds) < 2 ? "0".$seconds : $seconds;

            return $mins."m ".$seconds."s";
            
        } else {
            return $value." ".$unitLabel;

        }

        return $value;
    }
}