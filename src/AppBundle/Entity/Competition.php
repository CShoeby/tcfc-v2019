<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Competition
 *
 * @ORM\Table(name="competition")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompetitionRepository")
 */
class Competition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="workoutDeadline", type="datetime", nullable=true)
     */
    private $workoutDeadline;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var bool
     *
     * @ORM\Column(name="current", type="boolean")
     */
    private $current;

    /**
     * @var int
     *
     * @ORM\Column(name="minimumPointsPerDay", type="integer", nullable=true)
     */
    private $minimumPointsPerDay;

    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="competition")
     */
    private $events;

    public function __toString() 
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Competition
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Competition
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Competition
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set workoutDeadline
     *
     * @param \DateTime $workoutDeadline
     *
     * @return Competition
     */
    public function setWorkoutDeadline($workoutDeadline)
    {
        $this->workoutDeadline = $workoutDeadline;

        return $this;
    }

    /**
     * Get workoutDeadline
     *
     * @return \DateTime
     */
    public function getWorkoutDeadline()
    {
        return $this->workoutDeadline;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Competition
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set current
     *
     * @param boolean $current
     *
     * @return Competition
     */
    public function setCurrent($current)
    {
        $this->current = $current;

        return $this;
    }

    /**
     * Get current
     *
     * @return bool
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Set minimumPointsPerDay
     *
     * @param integer $minimumPointsPerDay
     *
     * @return Competition
     */
    public function setMinimumPointsPerDay($minimumPointsPerDay)
    {
        $this->minimumPointsPerDay = $minimumPointsPerDay;

        return $this;
    }

    /**
     * Get minimumPointsPerDay
     *
     * @return int
     */
    public function getMinimumPointsPerDay()
    {
        return $this->minimumPointsPerDay;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     *
     * @return Competition
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
}
