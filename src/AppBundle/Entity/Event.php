<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=11)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=10, nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="unitLabel", type="string", length=50, nullable=true)
     */
    private $unitLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="questionForForm", type="string", length=255, nullable=true)
     */
    private $questionForForm;

    /**
     * @ORM\ManyToOne(targetEntity="EventType")
     * @ORM\JoinColumn(name="eventType", referencedColumnName="id")
     */
    private $eventType;

    /**
     * @ORM\OneToOne(targetEntity="Event")
     * @ORM\JoinColumn(name="alternateForEvent", referencedColumnName="id")
     */
    private $alternateForEvent;

    /**
     * @var float
     *
     * @ORM\Column(name="pointsMultiplier", type="float", nullable=true)
     */
    private $pointsMultiplier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var int
     *
     * @ORM\Column(name="repeatEveryNDays", type="integer", nullable=true)
     */
    private $repeatEveryNDays;

    /**
     * @var int
     *
     * @ORM\Column(name="repeatNTimes", type="integer", nullable=true)
     */
    private $repeatNTimes;

    /**
     * @var int
     *
     * @ORM\Column(name="minimumThreshold", type="integer", nullable=true)
     */
    private $minimumThreshold;

    /**
     * @var int
     *
     * @ORM\Column(name="target", type="integer", nullable=true)
     */
    private $target;

    /**
     * @var int
     *
     * @ORM\Column(name="isGreatestValue", type="integer", nullable=true)
     */
    private $isGreatestValue;

    /**
    * @ORM\OneToMany(targetEntity="Workout", mappedBy="event")
    */
    private $workouts;

    /**
     * @ORM\ManyToOne(targetEntity="Competition", inversedBy="events")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id")
     */
    private $competition;

    /**
     * @var bool
     *
     * @ORM\Column(name="isDisplayed", type="boolean")
     */
    private $isDisplayed;

    public function __construct()
    {        
        $this->workouts = new ArrayCollection();
        $this->isDisplayed = true;
    }

    public function __toString() {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Event
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set unitLabel
     *
     * @param string $unitLabel
     *
     * @return Event
     */
    public function setUnitLabel($unitLabel)
    {
        $this->unitLabel = $unitLabel;

        return $this;
    }

    /**
     * Get unitLabel
     *
     * @return string
     */
    public function getUnitLabel()
    {
        return $this->unitLabel;
    }

    /**
     * Set questionForForm
     *
     * @param string $questionForForm
     *
     * @return Event
     */
    public function setQuestionForForm($questionForForm)
    {
        $this->questionForForm = $questionForForm;

        return $this;
    }

    /**
     * Get questionForForm
     *
     * @return string
     */
    public function getQuestionForForm()
    {
        return $this->questionForForm;
    }

    /**
     * Set competition
     *
     * @param \stdClass $competition
     *
     * @return Event
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;

        return $this;
    }

    /**
     * Get competition
     *
     * @return \stdClass
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * Set pointsMultiplier
     *
     * @param float $pointsMultiplier
     *
     * @return Event
     */
    public function setPointsMultiplier($pointsMultiplier)
    {
        $this->pointsMultiplier = $pointsMultiplier;

        return $this;
    }

    /**
     * Get pointsMultiplier
     *
     * @return float
     */
    public function getPointsMultiplier()
    {
        return $this->pointsMultiplier;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Event
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set minimumThreshold
     *
     * @param integer $minimumThreshold
     *
     * @return Event
     */
    public function setMinimumThreshold($minimumThreshold)
    {
        $this->minimumThreshold = $minimumThreshold;

        return $this;
    }

    /**
     * Get minimumThreshold
     *
     * @return int
     */
    public function getMinimumThreshold()
    {
        return $this->minimumThreshold;
    }

    /**
     * Set target
     *
     * @param integer $target
     *
     * @return Event
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return int
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Add workout
     *
     * @param \AppBundle\Entity\Workout $workout
     *
     * @return Event
     */
    public function addWorkout(\AppBundle\Entity\Workout $workout)
    {
        $this->workouts[] = $workout;

        return $this;
    }

    /**
     * Remove workout
     *
     * @param \AppBundle\Entity\Workout $workout
     */
    public function removeWorkout(\AppBundle\Entity\Workout $workout)
    {
        $this->workouts->removeElement($workout);
    }

    /**
     * Get workouts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkouts()
    {
        return $this->workouts;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Event
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set isGreatestValue
     *
     * @param integer $isGreatestValue
     *
     * @return Event
     */
    public function setIsGreatestValue($isGreatestValue)
    {
        $this->isGreatestValue = $isGreatestValue;

        return $this;
    }

    /**
     * Get isGreatestValue
     *
     * @return integer
     */
    public function getIsGreatestValue()
    {
        return $this->isGreatestValue;
    }

    /**
     * Set eventType
     *
     * @param \AppBundle\Entity\EventType $eventType
     *
     * @return Event
     */
    public function setEventType(\AppBundle\Entity\EventType $eventType = null)
    {
        $this->eventType = $eventType;

        return $this;
    }

    /**
     * Get eventType
     *
     * @return \AppBundle\Entity\EventType
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * Set repeatEveryNDays
     *
     * @param integer $repeatEveryNDays
     *
     * @return Event
     */
    public function setRepeatEveryNDays($repeatEveryNDays)
    {
        $this->repeatEveryNDays = $repeatEveryNDays;

        return $this;
    }

    /**
     * Get repeatEveryNDays
     *
     * @return integer
     */
    public function getRepeatEveryNDays()
    {
        return $this->repeatEveryNDays;
    }

    public function getDatesOfRepeatedEvent($format = 'm-d-Y')
    {
        $dates = array($this->getStartDate());
        
        $intervalDays = $this->getRepeatEveryNDays();

        for ($i=1; $i < $this->getRepeatNTimes() ; $i++) { 
            $dates[$i] = clone $dates[$i-1];
            $dates[$i]->add(date_interval_create_from_date_string("+".$intervalDays." days"));
        }

        if ($format) {
            foreach ($dates as $key => $date) {
                $dates[$key] = $date->format($format);
            }
        }

        return $dates;
    }

    /**
     * Set repeatNTimes
     *
     * @param integer $repeatNTimes
     *
     * @return Event
     */
    public function setRepeatNTimes($repeatNTimes)
    {
        $this->repeatNTimes = $repeatNTimes;

        return $this;
    }

    /**
     * Get repeatNTimes
     *
     * @return integer
     */
    public function getRepeatNTimes()
    {
        return $this->repeatNTimes;
    }

    /**
     * Set isDisplayed
     *
     * @param boolean $isDisplayed
     *
     * @return Event
     */
    public function setIsDisplayed($isDisplayed)
    {
        $this->isDisplayed = $isDisplayed;

        return $this;
    }

    /**
     * Get isDisplayed
     *
     * @return boolean
     */
    public function getIsDisplayed()
    {
        return $this->isDisplayed;
    }

    /**
     * Set alternateForEvent
     *
     * @param \AppBundle\Entity\Event $alternateForEvent
     *
     * @return Event
     */
    public function setAlternateForEvent(\AppBundle\Entity\Event $alternateForEvent = null)
    {
        $this->alternateForEvent = $alternateForEvent;

        return $this;
    }

    /**
     * Get alternateForEvent
     *
     * @return \AppBundle\Entity\Event
     */
    public function getAlternateForEvent()
    {
        return $this->alternateForEvent;
    }
}
