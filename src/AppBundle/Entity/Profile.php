<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\User;

/**
 * Profile
 *
 * @ORM\Table(name="profile")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProfileRepository")
 */
class Profile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\OneToOne(targetEntity="User", mappedBy="profile", cascade={"persist"})
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=15, nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="hometown", type="string", length=255, nullable=true)
     */
    private $hometown;

    /**
     * @var string
     *
     * @ORM\Column(name="favWorkout", type="string", length=255, nullable=true)
     */
    private $favWorkout;

    /**
     * @var string
     *
     * @ORM\Column(name="favAthlete", type="string", length=255, nullable=true)
     */
    private $favAthlete;

    public function __construct() 
    {
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     *
     * @return Profile
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Profile
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Profile
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Profile
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set hometown
     *
     * @param string $hometown
     *
     * @return Profile
     */
    public function setHometown($hometown)
    {
        $this->hometown = $hometown;

        return $this;
    }

    /**
     * Get hometown
     *
     * @return string
     */
    public function getHometown()
    {
        return $this->hometown;
    }

    /**
     * Set favWorkout
     *
     * @param string $favWorkout
     *
     * @return Profile
     */
    public function setFavWorkout($favWorkout)
    {
        $this->favWorkout = $favWorkout;

        return $this;
    }

    /**
     * Get favWorkout
     *
     * @return string
     */
    public function getFavWorkout()
    {
        return $this->favWorkout;
    }

    /**
     * Set favAthlete
     *
     * @param string $favAthlete
     *
     * @return Profile
     */
    public function setFavAthlete($favAthlete)
    {
        $this->favAthlete = $favAthlete;

        return $this;
    }

    /**
     * Get favAthlete
     *
     * @return string
     */
    public function getFavAthlete()
    {
        return $this->favAthlete;
    }
}
