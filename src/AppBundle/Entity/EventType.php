<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventType
 *
 * @ORM\Table(name="event_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventTypeRepository")
 */
class EventType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="canEnterWorkout", type="boolean",  options={"default" : true})
     */
    private $canEnterWorkout;

    public function __toString()
    {
        return $this->name;
    }
    
    public function __construct() 
    {
        $this->canEnterWorkout = true;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EventType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return EventType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set canEnterWorkout
     *
     * @param boolean $canEnterWorkout
     *
     * @return EventType
     */
    public function setCanEnterWorkout($canEnterWorkout)
    {
        $this->canEnterWorkout = $canEnterWorkout;

        return $this;
    }

    /**
     * Get canEnterWorkout
     *
     * @return boolean
     */
    public function getCanEnterWorkout()
    {
        return $this->canEnterWorkout;
    }
}
