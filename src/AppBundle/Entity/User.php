<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use AppBundle\Entity\Profile;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\OneToMany(targetEntity="Workout", mappedBy="user")
    */
    private $workouts;

    /**
     * @var \stdClass
     *
     * @ORM\OneToOne(targetEntity="Profile", inversedBy="user", cascade={"persist"})
     */
    private $profile;

    public function __construct()
    {
        parent::__construct();
        
        $this->profile = new Profile();
        $this->workouts = new ArrayCollection();
    }

    public function __toString(){
        return $this->getUsername();
    }

    /**
     * Add workout
     *
     * @param \AppBundle\Entity\Workout $workout
     *
     * @return User
     */
    public function addWorkout(\AppBundle\Entity\Workout $workout)
    {
        $this->workouts[] = $workout;

        return $this;
    }

    /**
     * Remove workout
     *
     * @param \AppBundle\Entity\Workout $workout
     */
    public function removeWorkout(\AppBundle\Entity\Workout $workout)
    {
        $this->workouts->removeElement($workout);
    }

    /**
     * Get workouts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkouts()
    {
        return $this->workouts;
    }

    /**
     * Set profile
     *
     * @param \AppBundle\Entity\Profile $profile
     *
     * @return User
     */
    public function setProfile(\AppBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \AppBundle\Entity\Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }


    public function getGender()
    {
        return $this->profile->getGender();
    }
}
