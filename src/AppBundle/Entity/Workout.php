<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Event;
use AppBundle\Entity\User;
use AppBundle\Service\WorkoutHelper;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Workout
 *
 * @ORM\Table(name="workout")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WorkoutRepository")
 */
class Workout
{

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
           $metadata->addPropertyConstraint('value', new Assert\GreaterThan([
            'value' => 0,
        ]));
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="workouts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="workouts")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    private $event;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="workoutDate", type="datetime")
     */
    private $workoutDate;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enteredOn", type="datetime")
     */
    private $enteredOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deletedOn", type="datetime", nullable=true)
     */
    private $deletedOn;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="float", nullable=true)
     */
    private $value;

    public function __construct(User $user, Event $event, $workoutDate = null) {
        $this->setUser($user);
        $this->setEvent($event);

        if(!is_null($workoutDate)) {
            $this->setWorkoutDate(new \DateTime($workoutDate));
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     *
     * @return Workout
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set event
     *
     * @param \stdClass $event
     *
     * @return Workout
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \stdClass
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set workoutDate
     *
     * @param \DateTime $workoutDate
     *
     * @return Workout
     */
    public function setWorkoutDate($workoutDate)
    {
        $this->workoutDate = $workoutDate;

        return $this;
    }

    /**
     * Get workoutDate
     *
     * @return \DateTime
     */
    public function getWorkoutDate()
    {
        return $this->workoutDate;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Workout
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set enteredOn
     *
     * @param \DateTime $enteredOn
     *
     * @return Workout
     */
    public function setEnteredOn($enteredOn)
    {
        $this->enteredOn = $enteredOn;

        return $this;
    }

    /**
     * Get enteredOn
     *
     * @return \DateTime
     */
    public function getEnteredOn()
    {
        return $this->enteredOn;
    }

    /**
     * Set deletedOn
     *
     * @param \DateTime $deletedOn
     *
     * @return Workout
     */
    public function setDeletedOn($deletedOn)
    {
        $this->deletedOn = $deletedOn;

        return $this;
    }

    /**
     * Get deletedOn
     *
     * @return \DateTime
     */
    public function getDeletedOn()
    {
        return $this->deletedOn;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return Workout
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function setValueSeconds($valueSeconds)
    {
        $this->value = $valueSeconds->i * 60 + $valueSeconds->s;
        return $this;
    }

    public function getValueSeconds()
    {
        $minutes = floor($this->value / 60);
        $seconds = $this->value % 60;

        return new \DateInterval( "P0Y0M0DT0H".$minutes."M".$seconds."S" );
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    public function getPoints()
    {
        return $this->value;
    }

    public function getPrettyValue()
    {
        $workoutHelper = new WorkoutHelper();
        return $workoutHelper->getPrettyValue($this->value, $this->getEvent()->getUnitLabel());
    }

}
