<?php
// src/AppBundle/Twig/AppExtension.php
namespace AppBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('prettyTime', [$this, 'formatTime']),
        );
    }

    public function formatTime($value)
    {
        //convert seconds to minutes and seconds
        $mins = floor($value / 60);
        $seconds = $value % 60;
        $seconds = strlen($seconds) < 2 ? "0".$seconds : $seconds;

        return $mins."m ".$seconds."s";
            
        var_dump($value);die;
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }
}